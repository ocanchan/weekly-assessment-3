const examData = [
  { id: 1, name: "Tony", score: 87 },
  { id: 2, name: "Steve", score: 91 },
  { id: 3, name: "Scott", score: 72 },
  { id: 4, name: "Natasha", score: 66 },
  { id: 5, name: "Bruce", score: 77 },
  { id: 6, name: "Denvers", score: 82 },
  { id: 7, name: "Pepper", score: 91 },
  { id: 8, name: "Clint", score: 84 },
  { id: 9, name: "Barton", score: 90 },
  { id: 10, name: "Stacey", score: 88 },
  { id: 11, name: "Wanda", score: 50 },
  { id: 12, name: "Peter", score: 79 },
  { id: 13, name: "James", score: 84 },
  { id: 14, name: "Shang", score: 85 },
];

// You can modify the variable name
let averageScore = (data) => {
  let score = data.reduce((totalScore, value) => totalScore + value.score, 0) / data.length;
  return score;
};

// averageScore(examData)

let highestStudents = (data) => {
  const filtered = data.map(x => x.score)
  const maxNum = Math.max(...filtered)
  const highest = data.filter(x => x.score === maxNum)
  const nameOnly = highest.map(x => x.name)
  return nameOnly;
};

// highestStudents(examData)

let lowestStudents = (data) => {
  const filtered = data.map(x => x.score)
  const minNum = Math.min(...filtered)
  const lowest = data.filter(x => x.score === minNum)
  const nameOnly = lowest.map(x => x.name)
  return nameOnly;
};

// lowestStudents(examData)


let failStudents = (data) => {
  const score = data.filter(x => x.score < 75).map(x => x.name)
  return score;
}

// failStudents(examData)


let passStudents = (data) => {
  let score = data.filter((x) => x.score > 75).length / data.length * 100;
  return `${Math.round(score)}%`;
};

// passStudents(examData)


console.log(`Average score: ${averageScore(examData)}`);
console.log(`Student who get highest score: ${highestStudents(examData)}`);
console.log(`Student who get lowest score: ${lowestStudents(examData)}`);
console.log(`Student who fail: ${failStudents(examData)}`);
console.log(`Pass percentage: ${passStudents(examData)}`);

